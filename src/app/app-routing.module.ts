import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  {
    path:'',
    redirectTo:'home',
    pathMatch:'full'
  },

  { path: 'home',
   loadChildren: () => import('./components/pages/home/home.module').then(m => m.HomeModule) 
  }, 
  
  { path: 'personajes', loadChildren: () => import('./components/pages/personajes/personajes.module').then(m => m.PersonajesModule) },

  { path: 'episodios', loadChildren: () => import('./components/pages/episodios/episodios.module').then(m => m.EpisodiosModule) },

  { path: 'character-list',
   loadChildren: () => import('./components/pages/characters/character-list/character-list.module').then(m => m.CharacterListModule) 
  }, 
  
  { path: 'character-details/:id',
   loadChildren: () => import('./components/pages/characters/character-details/character-details.module').then(m => m.CharacterDetailsModule) 
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
