import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Character } from '@shared/interfaces/character.interface';
import { environment } from '@environment/environment';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(private http: HttpClient) { }
  
  buscarCharacters(query='', page = 1){
    const filter = `${environment.baseUrlAPI}/character/?name=${query}&page=${page}`;
    return this.http.get<Character[]>(filter);
  }

  obtenerDetails(id: number){
    return this.http.get<Character>(
      `${environment.baseUrlAPI}/character/${id}`
    );
  }
  
}
